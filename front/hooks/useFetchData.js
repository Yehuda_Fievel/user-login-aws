import { useState } from 'react'
import axios from 'axios'
import { SERVER_URL } from '../config'


const useFetchData = () => {
	const [data, setData] = useState(null)
	const [loading, setLoading] = useState(false)

	async function sendRequest(method, path, postBody, contentType) {
		setLoading(true)
		try {
			const request = { method, url: SERVER_URL + path }
			if (postBody) {
				request.data = postBody
			}
			if (contentType) {
				request.headers = { ['Content-Type']: contentType }
				request.data = new FormData()
				request.data.append('picture', postBody.picture[0].originFileObj)
				request.data.append('bio', postBody.bio)
			}
			if (localStorage.getItem('token')) {
				request.headers = { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}
			const result = await axios(request)
			setData(result.data)
			// setAlert({ message: 'success', type: 'success' })
		} catch (e) {
			// setAlert({ message: e.message, type: 'error' })
			console.log(e)
		}
		setLoading(false)
	}
	return { data, loading, sendRequest }
}

export default useFetchData