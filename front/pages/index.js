import { Form, Input, Button } from 'antd'
import useFetchData from '../hooks/useFetchData'

const layout = {
	labelCol: {
		span: 8,
	},
	wrapperCol: {
		span: 10,
	},
}
const tailLayout = {
	wrapperCol: {
		offset: 8,
		span: 16,
	},
}

// TODO: show loading bar while making API request
// TODO: show server side response messages to inform user of status/instructions

export default function Home() {
	const { sendRequest } = useFetchData()
	const onFinish = (values) => {
		sendRequest('POST', '/users/register', values)
	}

	return (
		<Form
			{...layout}
			name='login'
			onFinish={onFinish}
		>
			<Form.Item
				label='Name'
				name='name'
				rules={[
					{
						required: true,
						message: 'Please input your name!',
					}
				]}
			>
				<Input />
			</Form.Item>
			<Form.Item
				label='Email'
				name='email'
				rules={[
					{
						required: true,
						message: 'Please input your email!',
					},
					{
						type: 'email',
						message: 'Please enter a valid email!',
					}
				]}
			>
				<Input />
			</Form.Item>
			<Form.Item
				label='Password'
				name='password'
				rules={[
					{
						required: true,
						message: 'Please input your password!',
					},
				]}
			>
				<Input.Password />
			</Form.Item>
			<Form.Item {...tailLayout}>
				<Button type='primary' htmlType='submit'>Submit</Button>
			</Form.Item>
		</Form>
	)
}
