import { useState, useEffect } from 'react'
import { Form, Upload, Input, Button } from 'antd'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import useFetchData from '../hooks/useFetchData'


const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}

// TODO: send signed url link to upload picture directly to s3
// TODO: create route to view picture after upload to s3

export default function Profile() {
    const [id, setId] = useState(null)
    const [imageUrl, setImageUrl] = useState(null)
    const [loading, setLoading] = useState(false)
    const { sendRequest } = useFetchData()

    useEffect(() => {
        const params = new URLSearchParams(window.location.search)
        setId(params.get('id'))
    }, [])

    const normFile = e => {
        if (Array.isArray(e)) {
            return e
        }
        return e && e.fileList
    }


    const getBase64 = (img, callback) => {
        const reader = new FileReader()
        reader.addEventListener('load', () => callback(reader.result))
        reader.readAsDataURL(img)
    }

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true)
            return
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, imageUrl => {
                setImageUrl(imageUrl)
                setLoading(false)
            })
        }
    }

    const onFinish = values => {
        console.log('Received values of form: ', values)
        sendRequest('PUT', `/users/${id}`, values, 'multipart/form-data')
    }

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    )

    return (
        <Form
            name='update'
            {...formItemLayout}
            onFinish={onFinish}
        >
            <Form.Item
                name='picture'
                label='Picture'
                valuePropName='fileList'
                getValueFromEvent={normFile}
            >
                <Upload
                    name='picture'
                    listType='picture-card'
                    showUploadList={false}
                    onChange={handleChange}
                >
                    {imageUrl ? <img src={imageUrl} alt='avatar' style={{ width: '100%' }} /> : uploadButton}
                </Upload>
            </Form.Item>
            <Form.Item
                name='bio'
                label='Bio'
            >
                <Input.TextArea />
            </Form.Item>
            <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                <Button type='primary' htmlType='submit'>Submit</Button>
            </Form.Item>
        </Form>
    )
}
