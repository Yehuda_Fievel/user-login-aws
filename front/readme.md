## Setup

- add AWS env variables to .env file based on the .env.sample file
- `docker build --tag user-login-aws-front .`


## Run

- `docker run -p 3000:3000 -d user-login-aws-front`