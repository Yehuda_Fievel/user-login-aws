const querystring = require('querystring');
const awsDB = require('../services/awsDynamoDB');
const awsSES = require('../services/awsSES');
const awsS3 = require('../services/awsS3');
const helpers = require('../utils/helpers');


async function register(req, res, next) {
    try {
        const id = helpers.createRamdonString(8);
        const code = helpers.createRamdonString(48);
        await awsDB.create(req.body, id, code);
        const url = `${process.env.SERVER_URL}/users/verifyEmail?id=${id}&code=${code}`;
        await awsSES.sendEmail(req.body.email, url, 'Welcome');
        res.status(201).json({ message: 'user created' });
    } catch (error) {
        next(error);
    }
}


async function verifyEmail(req, res, next) {
    try {
        const user = await awsDB.getItemByCode(req.query);
        const queryString = querystring.stringify({
            id: user.Item.id,
            name: user.Item.name,
            email: user.Item.email,
        });
        console.log(`${process.env.ORIGIN_URL}/profile?${queryString}`);
        res.status(304).redirect(`${process.env.ORIGIN_URL}/profile?${queryString}`);
    } catch (error) {
        next(error);
    }
}

async function update(req, res, next) {
    try {
        const key = `${req.params.id}/${Date.now()}`;
        await awsS3.uploadFile(key, req.file.path);
        // update db
        res.status(200).json({ message: 'profile updated' });
    } catch (error) {
        next(error);
    }
}


module.exports = {
    register,
    verifyEmail,
    update,
}