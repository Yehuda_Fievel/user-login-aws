require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 4000;

// TODO: add morgan and winston for logging

app.use(cors());
app.use(bodyParser.json());

require('./routes')(app);


process.on('unhandledRejection', error => {
    console.log('unhandledRejection: ' + error.message);
    console.log(error);
});

process.on('uncaughtException', (err, origin) => {
    console.log(`Exception origin: ${origin}`);
    console.log(`Caught exception: ${err}`);
});


app.listen(port, () => { console.log(`Example app listening on ${port}`) });