## Setup

- add AWS env variables to .env file based on the .env.sample file
- `docker build --tag user-login-aws-server .`


## Run

- `docker run -p 4000:4000 -d user-login-aws-server`