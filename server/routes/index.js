const { request } = require('express')

module.exports = function (app) {
    app.use('/users', require('./users'))


    // Handles 404 request if no matching route to avoid sending back default express error
    app.use((req, res, next) => {
        const error = new Error('You do not belong here');
        console.log(error);
        res.status(404).send(error.message);
    });

    // Error Handler
    app.use(async (err, req, res, next) => {
        console.log(err);
        res.status(err.statusCode || 400).json({ error: true, message: err.message });
    });
}