const express = require('express');
const router = express.Router();
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })
const userSchema = require('../schema/users');
const userCtrl = require('../controllers/users.js')

// TODO: add JOI validation to the other routes

router.post('/register', userSchema.register, userCtrl.register);

router.get('/verifyEmail', userCtrl.verifyEmail);

router.put('/:id', upload.single('picture'), userCtrl.update);

module.exports = router;