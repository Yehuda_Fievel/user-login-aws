const AWS = require('aws-sdk');
const helpers = require('../utils/helpers');

// AWS.config.update({ region: process.env.aws_region });

const documentClient = new AWS.DynamoDB.DocumentClient();


async function create(user, id, code) {
    try {
        const params = {
            TableName: 'users',
            Item: {
                id,
                name: user.name,
                email: user.email,
                password: helpers.hashPassword(user.password),
                code,
            },
        };
        return documentClient.put(params).promise();
    } catch (error) {
        throw error;
    }
}


async function getItemByCode({ id, code }) {
    try {
        const params = {
            TableName: 'users',
            Key: {
                // should search using the code
                // maybe also delete the code after the account is verrified
                id, /* code, */
            },
        };
        return documentClient.get(params).promise();
    } catch (error) {
        throw error;
    }
}


module.exports = {
    create,
    getItemByCode,
}