const AWS = require('aws-sdk');
const util = require('util');
const fs = require('fs');


const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

const s3PutObject = util.promisify(s3.putObject.bind(s3));
const s3GetObject = util.promisify(s3.getObject.bind(s3));


async function uploadFile(key, file) {
    if (typeof file == 'string') {
        file = fs.readFileSync(file);
    }
    try {
        const params = {
            Bucket: process.env.AWS_BUCKET,
            Key: key,
            Body: file,
        };
        const data = await s3PutObject(params);
        return data;
    } catch (e) {
        throw e;
    }
}


async function downloadFile(key) {
    try {
        var params = {
            Bucket: process.env.AWS_BUCKET,
            Key: key,
        };
        const data = await s3GetObject(params);
        return data;
    } catch (e) {
        throw e;
    }
}


module.exports = {
    uploadFile,
    downloadFile,
}