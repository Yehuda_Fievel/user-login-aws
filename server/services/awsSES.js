var AWS = require('aws-sdk');


async function sendEmail(to, text, subject) {
    try {
        var params = {
            Destination: {
                ToAddresses: [
                    to,
                ]
            },
            Message: {
                Body: {
                    // TODO: create HTML tempalte
                    // Html: {
                    //     Charset: 'UTF-8',
                    //     Data: 'HTML_FORMAT_BODY'
                    // },
                    Text: {
                        Charset: 'UTF-8',
                        Data: text,
                    }
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: subject,
                }
            },
            Source: process.env.SEND_EMAIL,
        };
        return new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
    } catch (error) {
        throw error;
    }
}


module.exports = {
    sendEmail,
}