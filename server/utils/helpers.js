const crypto = require('crypto');
const bcrypt = require('bcryptjs');


/**
 * Create ramdom string
 * @param {number} num
 * @returns random string
 */
function createRamdonString(num) {
    return crypto.randomBytes(num).toString('hex');
}


/**
 * Hash password using bycrpt
 * @param {string} password
 * @returns password hash 
 */
function hashPassword(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}


module.exports = {
    createRamdonString,
    hashPassword,
}